const fetch=require('node-fetch');
const readline = require('readline');
var lector= readline.createInterface({
    input:  process.stdin,
    output: process.stdout
});
const dataApi = require('./dataApi');

function menu(){

    console.log ('MENU');
    console.log ('1.- Actualizar indicadores');
    console.log ('2.- Promediar ');
    console.log ('3.- Mostrar valor mas actual ');
    console.log ('4.- Mostrar minimo historico');
    console.log ('5.- Mostrar maximo historico');
    console.log ('6.- Salir');
    lector.question('Escriba su opción: ', opcion => {
        switch(opcion){
            case '1':
                dataApi.SolicitarDatos().then(
                    res=>{
                        console.log('se cargaron los datos con exito\n');
                        menu();
                    }
                ).catch(err=>{console.log('error al realizar la peticion')})
                
                break;
            case '2':
                dataApi.promediar().then(
                    res=>{
                        console.log('\n');
                        menu();
                    }
                ).catch(err=>{console.log('error al realizar la peticion')})
                break;
            case '3':
                dataApi.actual().then(
                    res=>{
                        console.log('\n');
                        menu();
                    }
                 ).catch(err=>{console.log('error al realizar la peticion')})
               
                break; 
            case '4':
                    dataApi.minimo().then(
                        res=>{
                            console.log('\n');
                            menu();
                        }
                     ).catch(err=>{console.log('error al realizar la peticion')})
            
                break; 
            case '5':
                    dataApi.maximo().then(
                        res=>{
                            console.log('\n');
                            menu();
                        }
                     ).catch(err=>{console.log('error al realizar la peticion')})
        
                break;     
            case '6':
                    console.log("Cerrando la aplicación");
                    lector.close();
                    process.exit(0);
                    break;

            default:
                console.log('opción no encontrada');
                console.log("Debe ingresar una opcion valida");
                menu();
                break;       
        }
    });
    
}
menu();
